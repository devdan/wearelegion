# Wearelegion 🔢

Loan Calculator

## Getting Started

Copy the repository with the code to the local machine

```
git clone git@gitlab.com:devdan/wearelegion.git
```

```
cd Wearelegion && code . && npm install && npm start
```



## Running the tests

The project has 2 configuration files for styles and scripts, respectively.
The project uses eslitnt and stylelint

```
npm test
```

## Author

* **Danil Chernov** - [🔗 VK](https://vk.com/id51046891)
* **Artem Khaov** -  [🔗 VK](https://vk.com/khaov)

