import '../config/jqueryLoad';
import 'jquery-countdown';

let decCache = [],
  decCases = [2, 0, 1, 1, 1, 2];

function decOfNum(number, titles) {
  if (!decCache[number])
    decCache[number] =
      number % 100 > 4 && number % 100 < 20
        ? 2
        : decCases[Math.min(number % 10, 5)];

  return titles[decCache[number]];
}

let textDeclination = options => {
  let { totalDays, hours, minutes } = options;
  let result = {};

  result['days'] = {
    value: totalDays,
    unit: decOfNum(totalDays, ['день', 'дня', 'дней']),
  };

  result['hours'] = {
    value: hours,
    unit: decOfNum(hours, ['час', 'часа', 'часов']),
  };

  result['minutes'] = {
    value: minutes,
    unit: decOfNum(minutes, ['минута', 'минуты', 'минут']),
  };

  return result;
};

export default () => {
  let $timerContainers = $('.js-timer-container');

  if ($timerContainers.length) {
    $timerContainers.each(function() {
      let $timerContainer = $(this);
      let $timer = $timerContainer.find('.js-timer');

      let finalDate = $timer.data('date');

      $timer
        .countdown(finalDate)
        .on('update.countdown', function(event) {
          let typedText = textDeclination(event.offset);

          for (let key in typedText) {
            let data = typedText[key];

            let $timerItem = $timer.find(`.js-${key}`);

            let $value = $timerItem.find('.timer__value').text(data.value);

            let $unit = $timerItem.find('.timer__unit').text(data.unit);
          }
        })
        .on('finish.countdown', function(event) {
          let $notifyButton = $timerContainer
            .find('.js-button-notify')
            .addClass('is-hidden');

          let $buyButton = $timerContainer
            .find('.js-button-buy')
            .removeClass('is-hidden');

          let $price = $timerContainer
            .find('.js-good-price')
            .removeClass('is-hidden');

          $timer.remove();
        });
    });
  }
};
