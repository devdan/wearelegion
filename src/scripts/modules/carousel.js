import '../config/jqueryLoad';
import 'owl.carousel';

export default class {
  constructor(props) {
    this.state = {
      isCarousel: false,
      breakpoint: 1275,
      carousel: $('.js-carousel'),
    };
  }

  changeLayout() {
    let { isCarousel, carousel } = this.state;

    let classNames = {
      default: 'default-state',
      carousel: 'owl-carousel carousel-state',
    };

    if (isCarousel) {
      carousel.removeClass(classNames.default);
      carousel.addClass(classNames.carousel);
    } else {
      carousel.removeClass(classNames.carousel);
      carousel.addClass(classNames.default);
    }
  }

  toggleCarousel() {
    let { isCarousel } = this.state;

    let options = {
      autoWidth: true,
      responsive: {
        320: {
          margin: 15,
        },
        540: {
          margin: 20,
        },
      },
    };

    let $carousel = this.state.carousel;

    if (isCarousel) {
      $carousel.owlCarousel(options);
    } else {
      $carousel.trigger('destroy.owl.carousel');
    }
  }

  onResizeHandler() {
    let $window = $(window);

    $window.resize(() => this.checkWindowWidth());
  }

  checkWindowWidth() {
    let $window = $(window);
    let { breakpoint } = this.state;

    let windowWidth = $window.width();

    if (windowWidth > breakpoint) {
      this.state.isCarousel = false;
    } else {
      this.state.isCarousel = true;
    }

    this.changeLayout();
    this.toggleCarousel();
  }

  run() {
    if (this.state.carousel.length) {
      this.checkWindowWidth();

      this.onResizeHandler();
    }
  }
}
