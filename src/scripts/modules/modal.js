import '../config/jqueryLoad';
import '@fancyapps/fancybox';

let defaults = {
  closeExisting: true,
  // hideScrollbar: true,
  touch: false,
  btnTpl: {
    smallBtn: '',
  },
};

export let togglingPreviewModal = () => {
  let $openButtons = $('.js-preview-modal');
  if ($openButtons.length) {
    $openButtons.each(function() {
      let $openButton = $(this);

      let options = {};
      Object.assign(options, defaults);

      $openButton.fancybox(options);
    });
  }
};

export let togglingConfirmModal = () => {
  let $openButtons = $('.js-confirm-modal');
  if ($openButtons.length) {
    $openButtons.each(function() {
      let $openButton = $(this);

      let options = {};
      Object.assign(options, defaults);

      $openButton.fancybox(options);
    });
  }
};

export let togglingProcessingModal = () => {
  let $openButtons = $('.js-processing-modal');
  if ($openButtons.length) {
    $openButtons.each(function() {
      let $openButton = $(this);

      let options = {};
      Object.assign(options, defaults);

      $openButton.fancybox(options);
    });
  }
};

export let togglingNavigationModal = () => {
  let $openButtons = $('.js-navigation-modal');
  if ($openButtons.length) {
    $openButtons.each(function() {
      let $openButton = $(this);

      let options = {
        beforeShow: function() {
          $(document.body).addClass('navigation-is-open');
        },
        afterClose: function() {
          $(document.body).removeClass('navigation-is-open');
        },
      };
      Object.assign(options, defaults);

      $openButton.fancybox(options);
    });
  }
};
