import * as THREE from 'three';

export default function smokeTop() {
  let smokeCanvas = document.getElementById('smokeAtTop');
  if (smokeCanvas === null) return false;
  let width = document.documentElement.clientWidth;
  let height = 420;

  let scene = new THREE.Scene(); // задаю сцену
  let camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 10000); // задаю камеру
  let renderer = new THREE.WebGLRenderer({
    alpha: true,
    canvas: window.smokeAtTop,
  }); // объект для отрисовки
  renderer.setClearColor(0x000000, 0); // the default
  let clock = new THREE.Clock();

  renderer.setSize(width, height);
  // document.body.appendChild(renderer.domElement);

  camera.position.z = 1000;

  let light = new THREE.DirectionalLight(0x85c1ff, 1);
  light.position.set(0, 0, 1000);
  scene.add(light);

  let smokeTexture = new THREE.TextureLoader().load('/images/bg/smoke.png');
  let smokeMaterial = new THREE.MeshLambertMaterial({
    color: 0x85c1ff,
    map: smokeTexture,
    transparent: true,
  });
  smokeMaterial.opacity = 0.4;
  let smokeGeo = new THREE.PlaneGeometry(256, 256);
  let smokeParticles = [];

  /***** Построение дыма *****/

  /***** 1 шаг - верхний слой *****/
  let topSmokeCount = 50;
  let topIterationWidth = width / topSmokeCount;
  for (let p = 0; p < topSmokeCount; p++) {
    let particle = new THREE.Mesh(smokeGeo, smokeMaterial);
    particle.position.set(-width / 2 + p * topIterationWidth, 128, 800);
    particle.rotation.z = p * 0.3 * 360;
    scene.add(particle);
    smokeParticles.push(particle);
  }

  /***** 2 шаг - средний слой *****/
  let middleSmokeCount = 40;
  let middlePaddingLeft = 128;
  let middleIterationWidth = (width - middlePaddingLeft) / middleSmokeCount;
  for (let p = 0; p < middleSmokeCount; p++) {
    let particle = new THREE.Mesh(smokeGeo, smokeMaterial);
    particle.position.set(
      -width / 2 + p * middleIterationWidth + middlePaddingLeft,
      80,
      800 - p / 4,
    );
    particle.rotation.z = p * 0.3 * 360;
    scene.add(particle);
    smokeParticles.push(particle);
  }

  /***** 3 шаг - нижний слой *****/
  let bottomSmokeCount = 25;
  let bottomPaddingLeft = width / 4;
  let bottomIterationWidth = width / 2 / bottomSmokeCount;
  for (let p = 0; p < bottomSmokeCount; p++) {
    let particle = new THREE.Mesh(smokeGeo, smokeMaterial);
    if (p % 2 === 0) {
      particle.position.set(
        -width / 2 + p * bottomIterationWidth + bottomPaddingLeft,
        p / 2,
        850,
      );
      particle.rotation.z = -p * 0.25 * 360;
    } else {
      particle.position.set(
        -width / 2 + p * bottomIterationWidth + bottomPaddingLeft,
        -p / 2,
        825,
      );
      particle.rotation.z = p * 0.45 * 360;
    }
    scene.add(particle);
    smokeParticles.push(particle);
  }

  function animate() {
    // note: three.js includes requestAnimationFrame shim
    let delta = clock.getDelta();
    requestAnimationFrame(animate);
    evolveSmoke(delta);
    render();
  }

  function evolveSmoke(delta) {
    let sp = smokeParticles.length;
    while (sp--) {
      if (sp % 7 === 0) {
        smokeParticles[sp].rotation.z += -delta * 0.14;
      } else {
        smokeParticles[sp].rotation.z += delta * 0.19;
      }
    }
  }

  function render() {
    renderer.render(scene, camera);
  }

  animate();

  function onWindowResize() {
    width = document.documentElement.clientWidth;
    height = width > 992 ? 300 : 250;
    camera.aspect = document.documentElement.clientWidth / height;
    camera.updateProjectionMatrix();
    renderer.setSize(document.documentElement.clientWidth, height);
  }

  window.addEventListener('resize', onWindowResize, false);
}
