export default () => {
  let $filters = $('.js-filter');

  if ($filters.length) {
    $filters.each(function() {
      let $filter = $(this);
      let $options = $filter.find('.js-option');

      if ($options.length) {
        let $activeOption;
        let $items = $filter.find('.js-filter-item');

        $options.each(function() {
          let $option = $(this);

          if ($option.hasClass('is-active')) $activeOption = $option;

          let onClickHandler = event => {
            event.preventDefault();

            if (!$option.hasClass('is-active')) {
              $activeOption.removeClass('is-active');

              $activeOption = $option;
              $option.addClass('is-active');

              let value = $option.data('filterSelect');

              if (value === 'all') {
                $items.show();
              } else {
                $items.filter(`[data-filter-value='${value}']`).show();
                $items.filter(`:not([data-filter-value='${value}'])`).hide();
              }
            }
          };

          $option.off('click.filter').on('click.filter', onClickHandler);
        });
      }
    });
  }
};
