import '../config/jqueryLoad';

export default () => {
  let $buttons = $('.js-toggle-dropdown');

  if ($buttons.length) {
    $buttons.each(function() {
      let $button = $(this);
      let dropdownID = $button.data('dropdown');

      let onClickHandler = event => {
        event.preventDefault();
        let $dropdownItem = $(dropdownID);

        $button.toggleClass('is-open');
        $dropdownItem.slideToggle();
      };

      $button.off('click.dropdown').on('click.dropdown', onClickHandler);
    });
  }
};
