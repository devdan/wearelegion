import henlo from './modules/henlo';
import Carousel from './modules/Carousel';

import {
  togglingPreviewModal,
  togglingConfirmModal,
  togglingProcessingModal,
  togglingNavigationModal,
} from './modules/modal';

import dropdown from './modules/dropdown';
import timer from './modules/timer';
import filter from './modules/filter';

import smoke from './modules/smoke';

let carousel = new Carousel();
carousel.run();

togglingPreviewModal();
togglingConfirmModal();
togglingProcessingModal();
togglingNavigationModal();

dropdown();
timer();
filter();
smoke();
